/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
    extern volatile int flag;
    extern volatile int direction;
    extern volatile int prev_state;
int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    uint16 cnt;
    LCD_Start(); //start lcd screen
    ENC_ISR_Start(); //enable interrupts
    LCD_PrintString("VOLUME");
    cnt = 50;
    LCD_Position(1,0);
    LCD_PrintNumber(cnt);
    for(;;)
    {
        if (flag != 0 && direction == 0) //interrupt & cw turn
        {
            flag = 0; //clear flag for next potential interrupt
            cnt = cnt +1;
            if (cnt >= 100) cnt = 100; //max volume
            LCD_ClearDisplay();
            LCD_Position(0,0);
            LCD_PrintString("VOLUME");
            LCD_Position(1,0);
            LCD_PrintNumber(cnt);
        }
        if (flag != 0 && direction == 1) //interrupt & ccw turn
        {
            flag = 0; //clear flag for next potential interrupt
            cnt = cnt -1;
            if (cnt <= 0 || cnt >= 100 ) cnt = 0; //min volume (uint16 flips max when made negative)
            LCD_ClearDisplay();
            LCD_Position(0,0);
            LCD_PrintString("VOLUME");
            LCD_Position(1,0);
            LCD_PrintNumber(cnt);
        }                  
        
    }          
}
/* [] END OF FILE */
